const url = 'mongodb://localhost:27017/doctormanagment';
const mongoose = require('mongoose');
mongoose.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then( data =>{
    console.log("Database Connected");
});
// Models
require('./models/doctors');
require('./models/appointments');

exports.db = mongoose; 