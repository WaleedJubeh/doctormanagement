const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const appointmentSchema = new Schema({
    id: ObjectId,
    doctorId: Number,
    patientName: String,
    patientMobile: String,
    address: String,
    startHour: Date,
    endHour: Date,
});
mongoose.model("appointments", appointmentSchema);