const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const doctorSchema = new Schema({
    name: String,
    specilization: String,
    mobile: String,
    email: String,
    username: String,
    password: String,
});
mongoose.model("doctors", doctorSchema);