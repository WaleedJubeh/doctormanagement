
let database = require('./database').db;
function initApis(app) {
    const doctorModel = database.model('doctors');
    const appointmentsModel = database.model('appointments');
    app.post("/api/login", function (request, response) {
        const username = request.body.username;
        const password = request.body.password;
        console.log(username, password);
        response.render('login');
        let isfounded = true;
        if (isfounded) {
            return response.redirect('/dashboard');

        }
    });

    app.post("/api/appointment", function (request, response) {
        const appointment = {
            'doctorId': request.body.doctorId,
            'patientName': request.body.name,
            'patientMobile': request.body.mobile,
            'address': request.body.address,
            'startHour': request.body.startHour,
            'endHour': request.body.endHour,
        }
        const newAppointment = new appointmentsModel(doctor);
        newAppointment.save(function (err) {
            if (err) {
                console.log(err);

            } else
                console.log("SAVED");
            console.log(newAppointment);
        });
        return response.redirect(request.get('referer'));
    });
    app.post("/api/doctors", function (request, response) {
        console.log(request.body);
        let doctor = {
            'name': request.body.name,
            'specilization': request.body.specilization,
            'mobile': request.body.mobile,
            'email': request.body.email,
            'username': request.body.username,
            'password': request.body.password
        }

        const newDoctor = new doctorModel(doctor);
        newDoctor.save(function (err) {
            if (err) {
                console.log(err);

            } else
                console.log("SAVED");
            console.log(newDoctor);
        });
        return response.redirect(request.get('referer'));
    });
}
exports.initApis = initApis; 