document.addEventListener("DOMContentLoaded", (e) => {
    console.log("TTT");
    let button = Array.from(document.getElementsByClassName("close-modal"));
    let modal = document.getElementById("add-appointment");
    
    if(modal)
    {
        modal.addEventListener("focus", () => {
            modal.classList.remove("show");
        })
    }
    if (button) {
        button.forEach(btn => {
            btn.addEventListener("click", (e) => {
                
                if (modal)
                    modal.classList.remove("show");
            })
        })
    }
    let showModal = document.getElementById("show-modal");
    if (showModal)
        showModal.addEventListener("click", () => {
            if(modal)
            modal.classList.add("show");
        })
});