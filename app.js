
let database = require('./server/database');
let express = require('express');
let hbs = require('hbs');
let app = express();
api = require("./server/api.js");
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
hbs.registerPartials(__dirname + '/views/partials');

app.use(express.static('./public'));
app.use(express.urlencoded())

api.initApis(app);
// Routes
app.get('/', function(req, res) {
    res.render('login'); 
});
app.get('/dashboard', function(req, res) {
    res.render('dashboard',{modalAction:"/api/appointment"});
});
app.get('/doctors', function(req, res) {
    res.render('doctors');
});
  
app.listen(3000);  